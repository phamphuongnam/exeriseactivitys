package com.example.exceriseactivitys

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_date.*


class YearActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_date)
    }

    override fun onResume() {
        super.onResume()
//        buttonOke.isEnabled = false
//        editTextYear.addTextChangedListener(object : TextWatcher {
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                buttonOke.isEnabled = s.toString().trim { it <= ' ' }.length != 0
//            }
//
//            override fun beforeTextChanged(
//                s: CharSequence, start: Int, count: Int,
//                after: Int
//            ) {
//                Log.i("TAG", s.toString())
//            }
//
//            override fun afterTextChanged(s: Editable) {
//                Log.i("TAG", s.toString())
//            }
//        })
        buttonOke.setOnClickListener {
            val intent = Intent()
            intent.putExtra("KeyResultDate", editTextYear.text.toString().toInt())
            setResult(5, intent)
            finish()
        }
    }
}
